%% Copying T1 files elsewhere and preparing for CAT12 segmentation

% Assumed that data is organized in UKBB format (i.e. after applying the
% modified version of bb_file_manager
% Example,  maindir  = '/Users/psyc1586_admin/GVB_data/sample_subjects';
% wildcard = provide the common starting letter for subj names for example,
% sub, W, s, etc.

function arrange_for_CATSeg(maindir,wildcard)
    
    key      = sprintf('%s*',wildcard);
    subjdirs = dir(fullfile(maindir,key));
    subjdirs = subjdirs(~ismember({subjdirs.name},{'.','..'}));
    
    for sub = 1:length(subjdirs)
        
        subjdir  = fullfile(subjdirs(sub).folder,subjdirs(sub).name);
        cd(subjdir)
        subjname = subjdirs(sub).name;
        if ~exist(fullfile(subjdir,'T1'),'dir')
            warning('\n %s: T1 directory does not exist...Please check the data \n',subjname);...
            continue
        else
%             fprintf('\n %s: Creating a new folder with link to T1 file...\n',subjname);
            sourcefile = fullfile(subjdir,'T1', 'T1.nii.gz');
            destindir  = fullfile(subjdir,'CAT12_qc_work');
    
            if ~exist(destindir,'dir')
                mkdir(destindir);
            end

            if exist(fullfile(destindir,'T1.nii.gz'),'file') 
               warning('\n %s: The T1.nii.gz already exists...re-writing/re-copying the files \n',subjname);
            end

            copyfile(sourcefile,destindir);
            copyfile(strrep(sourcefile,'.nii.gz','.json'),destindir);
            fprintf('\n %s: File copying completed...\n',subjname);
        end
    end
end









    