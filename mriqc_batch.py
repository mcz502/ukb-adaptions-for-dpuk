#!/usr/bin/env python
# coding: utf-8

# In[14]:


import os
import sys
import glob
import shutil
import subprocess 
import shlex
from subprocess import Popen
import argparse
import json


# Example execution of the function for given subject's directory
# maindir         = "/home/fs0/mcz502/SampleData2/Moredata/test_mriqc_bbFM"
# bidsdirname     = "BIDS"
# qc_for          = "T1w" OR "T2w" OR "bold" OR "all"
# mriqcimg        = "/Scratch/mcz502/GVBwork/mriqc-0.15.1.simg"

# # qc_for: 'all', 'T1w', 'bold', 'T2w' can be given


#  Here's a function to run MRIQC on singularity
def mriqc_batch(maindir,bidsdirname,qc_for,mriqcimg):
    
    print('\n Preparing for MRIQC...We are checking the Subjects & Directories...Please Wait... \n')
        
    subjs = glob.glob(os.path.join(maindir,'*'))
    
    for sub in subjs:
        subjdir  = os.path.join(maindir,sub)
        subjname = os.path.basename(sub)

        if not os.path.isdir(os.path.join(subjdir,bidsdirname)):
            print("\n BIDS directory does not exist...Skipping the subject: %s \n" %subjname)
        elif not os.path.isdir(os.path.join(subjdir,bidsdirname,'sub-' + subjname)): 
            print("\n BIDS directory exists..but no data found: %s \n" %subjname)
        else:
            mainbids = os.path.join(subjdir,bidsdirname)
            subjbids = os.path.join(subjdir,bidsdirname,"sub-" + subjname)

            if qc_for == 'all':
                if not os.path.isdir(os.path.join(subjbids,'anat')) and os.path.isdir(os.path.join(subjbids,'func')):
                    print("Invalid option-all since either anat or func directory does not exist : %s" %subjname)
                else:
                    command = "singularity run " + mriqcimg + ' ' + mainbids + ' ' + mainbids + " participant" + " --verbose-reports"  
                    args    = shlex.split(command)
                    print('Starting the MRIQC pipeline now ...')
                    process = subprocess.Popen(args)

            elif qc_for == 'T1w':
                if not os.path.isdir(os.path.join(subjbids,'anat')):
                    print("Invalid option-all since either anat or func directory does not exist : %s" %subjname)
                else:
                    os.chdir(mainbids)
                    dummy_json("sub-" + subjname)
                    command = "singularity run " + mriqcimg + ' ' + mainbids + ' ' + mainbids + " participant" + ' -m ' + qc_for + " --verbose-reports" 
                    args    = shlex.split(command)
                    print('Starting the MRIQC pipeline now ...')
                    process = subprocess.Popen(args)

            elif qc_for == 'T2w':
                if not os.path.isdir(os.path.join(subjbids,'anat')):
                     print("Invalid option-all since either anat or func directory does not exist : %s" %subjname)
                else:
                    command = "singularity run " + mriqcimg + ' ' + mainbids + ' ' + mainbids + " participant" + ' -m ' + qc_for + " --verbose-reports" 
                    args    = shlex.split(command)
                    print('Starting the MRIQC pipeline now ...')
                    process = subprocess.Popen(args)

            elif qc_for == 'bold':
                if not os.path.isdir(os.path.join(subjbids,'anat')):
                    print("Invalid option-all since either anat or func directory does not exist : %s" %subjname)
                else:
                    command = "singularity run " + mriqcimg + ' ' + mainbids + ' ' + mainbids + " participant" + ' -m ' + qc_for + " --verbose-reports" 
                    args    = shlex.split(command)
                    print('Starting the MRIQC pipeline now ...')
                    process = subprocess.Popen(args)

            else:
                print("Invalid option provided: %s"%qc_for)        


def dummy_json(subjname):                
    # Data to be written
    dictionary = {
        "BIDSVersion": "1.0.0",
        "Name": subjname
    }

    # Serializing json
    json_object = json.dumps(dictionary, indent=4)

    # Writing to sample.json
    with open("dataset_description.json", "w") as outfile:
        outfile.write(json_object)                
                                
def print_dirs(var1,var2,var3,var5):
    print('\n This is directory main directory: %s \n'%var1)
    print('\n This is directory is BIDS directory name: %s \n'%var2)
    print('\n This is modality you want to QC: %s \n'%var3)
    print('\n This is path to singularity for mriqc: %s \n'%var5)
    
def main():
    parser = argparse.ArgumentParser(description='--Run MRIQC--')
    parser.add_argument("maindir", help = 'Parent Directory Name')
    parser.add_argument("bidsdirname", help = 'BIDS Directory Name')
    parser.add_argument("qc_for", help = 'QC Modality- T1w, T2w, bold')
    parser.add_argument("mriqcimg", help = 'MRIQC singularity image')
    
    args            = parser.parse_args()    
    maindir         = args.maindir
    
    bidsdirname     = args.bidsdirname
    qc_for          = args.qc_for
    mriqcimg        = args.mriqcimg
        
    print_dirs(maindir,bidsdirname,qc_for,mriqcimg)
    
    mriqc_batch(maindir,bidsdirname,qc_for,mriqcimg)    
    
if __name__ == "__main__":
    main()
