%% Run MRIQC on BIDS converted data by bb pipeline/ custom matlab script

%% This is a basic version; for finer version with more control, use the python version of the script

% % maindir         = '/Users/psyc1586_admin/remoteS/SampleData2/Moredata/test_mriqc_bbFM';
% % bidsdirname     = 'BIDS_matlabVer';
% % QC_for          = 'all'; % 'all', 'T1w', 'bold', 'T2w' can be given
% % datasåetdescript = '/Users/psyc1586_admin/remoteS/dataset_description.json';
% % mriqcimg        = '/Scratch/mcz502/GVBwork/mriqc-0.15.1.simg';

function mriqc_batch(maindir, bidsdirname, QC_for,datasetdescript,mriqcimg)
    subjs = dir(fullfile(maindir,'*'));
    subjs = subjs(~ismember({subjs.name},{'.','..'}));
    
    fprintf('\n Total %d subject found \n', length(subjs));
    
    for sub = 1:length(subjs)
        subjdir  = fullfile(subjs(sub).folder,subjs(sub).name);
        subjname = subjs(sub).name;
    
        if ~exist(fullfile(subjdir,bidsdirname),'dir')
            warning('\n BIDS directory does not exist..Skipping the subject : %s \n', subjname);
        elseif ~exist(fullfile(subjdir,'BIDS',['sub-',subjname]),'dir')
            error('\n BIDS directory exists...but no data found : %s \n', subjname);
        else     
            mainbids = fullfile(subjdir,bidsdirname);
            subjbids = fullfile(subjdir,bidsdirname,['sub-',subjname]);
            
            % Doing for all data
            if contains(QC_for,'all')
                disp(' Performing MRIQC for all the data.. ')
                if ~exist(fullfile(subjbids,'anat'),'dir') || ~exist(fullfile(subjbids,'func'),'dir')
                    error('\n Invalid option-all since either anat or func directory does not exist : %s', subjname);
                else
                    copyfile(datasetdescript,mainbids)
                    cmd = ['singulariy run ' mriqcimg ' ' mainbids ' ' mainbids ' participant'];
                    system(cmd)
                end
            % Doing for anat data
            elseif contains(QC_for,'T1w') || contains(QC_for,'T2w')
                disp(' Performing MRIQC for T1w data.. ')
                if ~exist(fullfile(subjbids,'anat'),'dir') 
                    error('\n Invalid option-T1w since anat directory does not exist : %s', subjname);
                else
                    copyfile(datasetdescript,mainbids)
                    cmd = ['singulariy run ' mriqcimg ' ' mainbids ' ' mainbids ' participant ' '-m ' QC_for];
                    system(cmd)
                end
            % Doing for func-bold data
            elseif contains(QC_for,'bold')
                disp(' Performing MRIQC for func data.. ')
                if ~exist(fullfile(subjbids,'func'),'dir') 
                    error('\n Invalid option-func since anat directory does not exist : %s', subjname);
                else
                    copyfile(datasetdescript,mainbids)
                    cmd = ['singulariy run ' mriqcimg ' ' mainbids ' ' mainbids ' participant ' '-m ' QC_for];
                    system(cmd)
                end
            % Check options 
            else
                error('Invalid option provided for %s...It must be either of these: T1w, T2w, bold, all', subjname);
            end
        end
    end
end

