%% UKBB to BIDS conversion: Without the json file

% subj_dir = '/Users/psyc1586_admin/GVB_data/sample_subjects/';
% subject  = 'W3T_2019_102_037_test';
% Date modified: 19th July 2022
% Warning: '&' in the filenames of SWI (channels) changed to 'and'; bash ln
% command fails with '&'
% Specifically, it does the conversion keeping in mind the BHC data
% acquisition

function ukbb_to_bids_matlabVersion(subj_dir,subject)    
    fullpath = fullfile(subj_dir,subject);
    
    cd(fullpath)
    
    
    if exist(fullfile(fullpath,'BIDS_matlabVer'),'dir')
        error('The BIDS folder already exists..Delete the existing directory or check if BIDS conversion is already done for %s', subject);
    else
        subject = ['sub-',subject];
        bidsdir = fullfile(fullpath,'BIDS_matlabVer',subject);
        mkdir(bidsdir);
        mkdir(fullfile(bidsdir,'anat'));
        mkdir(fullfile(bidsdir,'func'));
        mkdir(fullfile(bidsdir,'dwi'));
        
        % Converting the T1w data
        sourcedir = fullfile(fullpath,'T1');
        if isempty(sourcedir)
            warning('No T1w files were found for: %s ', subject)
        else
            sourcefile = fullfile(sourcedir,'T1.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_T1w.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
        end
        
    
        % Converting the T2 FLAIR data
        sourcedir = fullfile(fullpath,'T2_FLAIR');
        if isempty(sourcedir)
            warning('No T2_FLAIR files were found for: %s ', subject)
        else
            sourcefile = fullfile(sourcedir,'T2_FLAIR.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_FLAIR.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
        end
    
        % Converting the resting state data
        sourcedir = fullfile(fullpath,'fMRI');
        if isempty(sourcedir)
            warning('No fMRI files were found for: %s ', subject)
        else
            sourcefile = fullfile(sourcedir,'rfMRI.nii.gz');
            destindir  = fullfile(bidsdir,'func');
            destinfile = fullfile(destindir,[subject,'_task-rest_bold.nii.gz']);
    
            % including taskname in json
            jsonfile   = fullfile(sourcedir,'rfMRI.json');
            readjsonf  = fileread(jsonfile);
            strjsonf   = jsondecode(readjsonf);
            strjsonf.TaskName = 'rest';
            newjsonf   = jsonencode(strjsonf);        
            fid        = fopen(jsonfile,'w');
            fprintf(fid,'%s',newjsonf);
            fclose(fid);
    
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            cmd3       = ['ln -s ' strrep(sourcefile,'rfMRI','rfMRI_SBREF') ' ' strrep(destinfile,'_bold.nii.gz','_sbref.nii.gz')];
            cmd4       = ['ln -s ' strrep(sourcefile,'rfMRI.nii.gz','rfMRI_SBREF.json') ' ' strrep(destinfile,'_bold.nii.gz','_sbref.json')];
            system(cmd)
            system(cmd2)
            system(cmd3)
            system(cmd4)
        end
    
        % Converting the diffusion data
        sourcedir = fullfile(fullpath,'dMRI','raw');
        if isempty(sourcedir)
            warning('No dMRI files were found for: %s ', subject)
        else
            sourcefile = fullfile(sourcedir,'AP.nii.gz');
            destindir  = fullfile(bidsdir,'dwi');
            destinfile = fullfile(destindir,[subject,'_acq-AP_dwi.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            cmd3       = ['ln -s ' strrep(sourcefile,'nii.gz','bval') ' ' strrep(destinfile,'nii.gz','bval')];
            cmd4       = ['ln -s ' strrep(sourcefile,'nii.gz','bvec') ' ' strrep(destinfile,'nii.gz','bvec')];
            system(cmd)
            system(cmd2)
            system(cmd3)
            system(cmd4)
    
            sourcefile = fullfile(sourcedir,'PA.nii.gz');
            destindir  = fullfile(bidsdir,'dwi');
            destinfile = fullfile(destindir,[subject,'_acq-PA_dwi.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            cmd3       = ['ln -s ' strrep(sourcefile,'nii.gz','bval') ' ' strrep(destinfile,'nii.gz','bval')];
            cmd4       = ['ln -s ' strrep(sourcefile,'nii.gz','bvec') ' ' strrep(destinfile,'nii.gz','bvec')];
            system(cmd)
            system(cmd2)
            system(cmd3)
            system(cmd4)    
        end
    
        sourcedir = fullfile(fullpath,'raw');
        if isempty(sourcedir)
            warning('No dMRI files were found for: %s ', subject)
        else
            sourcefile = fullfile(sourcedir,'AP_SBREF.nii.gz');
            destindir  = fullfile(bidsdir,'dwi');
            destinfile = fullfile(destindir,[subject,'_acq-AP_sbref.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
            sourcefile = fullfile(sourcedir,'PA_SBREF.nii.gz');
            destindir  = fullfile(bidsdir,'dwi');
            destinfile = fullfile(destindir,[subject,'_acq-PA_sbref.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
        end
        
        % Converting SWI data
        sourcedir = fullfile(fullpath,'SWI');
        if isempty(sourcedir)
            warning('No SWI files were found for: %s ', subject)
        else
            sourcefile = fullfile(fullpath,'SWI','SWI_TOTAL_MAG.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_acq-mag-total_echo-1_SWI.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
    
            sourcefile = fullfile(fullpath,'SWI','SWI_TOTAL_MAG_TE2.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_acq-mag-total_echo-2_SWI.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
    
            sourcefile = fullfile(fullpath,'SWI','SWI_TOTAL_PHA.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_acq-pha-total_echo-1_SWI.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
    
            sourcefile = fullfile(fullpath,'SWI','SWI_TOTAL_PHA_TE2.nii.gz');
            destindir  = fullfile(bidsdir,'anat');
            destinfile = fullfile(destindir,[subject,'_acq-pha-total_echo-2_SWI.nii.gz']);
            cmd        = ['ln -s ' sourcefile ' ' destinfile];
            cmd2       = ['ln -s ' strrep(sourcefile,'nii.gz','json') ' ' strrep(destinfile,'nii.gz','json')];
            system(cmd)
            system(cmd2)
    
            e1_mag = dir(fullfile(fullpath,'SWI','MAG_TE1','*.nii.gz'));
            for files  = 1:length(e1_mag)
                
                sourcefile1  = fullfile(e1_mag(files).folder,e1_mag(files).name);
    
                newfile1     = strrep(sourcefile1,'&','and');
                newfile2     = strrep(strrep(sourcefile1,'&','and'),'.nii.gz','.json');
                
                try
                    movefile(sourcefile1,newfile1);            
                    movefile(strrep(sourcefile1,'.nii.gz','.json'),newfile2);
                end

                getch_no1   = strsplit(newfile1,'CH');
                getch_no    = strsplit(getch_no1{1,2},'_');
    
                if length(getch_no{1,1})<2
                    destinfile = fullfile(destindir,sprintf('%s_acq-mag-CH0%d_echo-1_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                else
                    destinfile = fullfile(destindir,sprintf('%s_acq-mag-CH%d_echo-1_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                end
                cmd       = ['ln -s ' newfile1 ' ' destinfile];
                cmd2      = ['ln -s ' newfile2 ' ' strrep(destinfile,'nii.gz','json')];
                system(cmd)
                system(cmd2)
            end
    
            e2_mag = dir(fullfile(fullpath,'SWI','MAG_TE2','*.nii.gz'));
            for files  = 1:length(e2_mag)
                
                sourcefile2  = fullfile(e2_mag(files).folder,e2_mag(files).name);
    
                newfile1     = strrep(sourcefile2,'&','and');
                newfile2     = strrep(strrep(sourcefile2,'&','and'),'.nii.gz','.json');
                
                try
                    movefile(sourcefile2,newfile1);            
                    movefile(strrep(sourcefile2,'.nii.gz','.json'),newfile2);
                end

                getch_no1   = strsplit(newfile1,'CH');
                getch_no    = strsplit(getch_no1{1,2},'_');
    
                if length(getch_no{1,1})<2
                    destinfile = fullfile(destindir,sprintf('%s_acq-mag-CH0%d_echo-2_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                else
                    destinfile = fullfile(destindir,sprintf('%s_acq-mag-CH%d_echo-2_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                end
                cmd       = ['ln -s ' newfile1 ' ' destinfile];
                cmd2      = ['ln -s ' newfile2 ' ' strrep(destinfile,'nii.gz','json')];
                system(cmd)
                system(cmd2)
            end
    
            e1_pha = dir(fullfile(fullpath,'SWI','pha_TE1','*.nii.gz'));
            for files  = 1:length(e1_pha)
                
                sourcefile1  = fullfile(e1_pha(files).folder,e1_pha(files).name);
    
                newfile1     = strrep(sourcefile1,'&','and');
                newfile2     = strrep(strrep(sourcefile1,'&','and'),'.nii.gz','.json');
                
                try
                    movefile(sourcefile1,newfile1);            
                    movefile(strrep(sourcefile1,'.nii.gz','.json'),newfile2);
                end
                
                getch_no1   = strsplit(newfile1,'CH');
                getch_no    = strsplit(getch_no1{1,2},'_');
    
                if length(getch_no{1,1})<2
                    destinfile = fullfile(destindir,sprintf('%s_acq-pha-CH0%d_echo-1_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                else
                    destinfile = fullfile(destindir,sprintf('%s_acq-pha-CH%d_echo-1_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                end
                cmd       = ['ln -s ' newfile1 ' ' destinfile];
                cmd2      = ['ln -s ' newfile2 ' ' strrep(destinfile,'nii.gz','json')];
                system(cmd)
                system(cmd2)
            end
    
            e2_pha = dir(fullfile(fullpath,'SWI','pha_TE2','*.nii.gz'));
            for files  = 1:length(e2_pha)
                
                sourcefile2  = fullfile(e2_pha(files).folder,e2_pha(files).name);
    
                newfile1     = strrep(sourcefile2,'&','and');
                newfile2     = strrep(strrep(sourcefile2,'&','and'),'.nii.gz','.json');
                
                try
                    movefile(sourcefile2,newfile1);            
                    movefile(strrep(sourcefile2,'.nii.gz','.json'),newfile2);
                end

                getch_no1   = strsplit(newfile1,'CH');
                getch_no    = strsplit(getch_no1{1,2},'_');
    
                if length(getch_no{1,1})<2
                    destinfile = fullfile(destindir,sprintf('%s_acq-pha-CH0%d_echo-2_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                else
                    destinfile = fullfile(destindir,sprintf('%s_acq-pha-CH%d_echo-2_SWI.nii.gz',subject, str2double(getch_no{1,1})));
                end
                cmd       = ['ln -s ' newfile1 ' ' destinfile];
                cmd2      = ['ln -s ' newfile2 ' ' strrep(destinfile,'nii.gz','json')];
                system(cmd)
                system(cmd2)
            end
        end
    end
end






