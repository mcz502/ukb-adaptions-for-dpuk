# This code runs the CAT12 segmentation (volumetric + surface) for T1 structural brain MRI data
# The CAT12 version is standalone(CAT12.8.1_r2042_R2017b_MCR_Linux)  
# The MATLAB 2017b (v 9.3) compiler is must for running 
# The jobs are submitted in array tasks using fsl_sub job manager
# It will overwrite any existing CAT12 runs for subjects


import os
import glob
import shutil
import argparse

# Prepare data for CAT12; copy T1.nii.gz from T1 folder to output folder
def prepare_cat(maindir,wildcard,outdir):    
    subjdirs = glob.glob(os.path.join(maindir,wildcard + '*'))
    for subjs in subjdirs:
        subjname = os.path.basename(subjs)    
        os.chdir(subjs)    
        if os.path.exists(os.path.join(subjs,'T1','T1_orig_defaced.nii.gz')):
            sourcefile = os.path.join(subjs,'T1','T1_orig_defaced.nii.gz')        
            if not os.path.exists(os.path.join(subjs,outdir)):
                os.mkdir(os.path.join(subjs,outdir))
            destindir  = os.path.join(subjs,outdir)        
            if not os.path.exists(os.path.join(destindir,'T1_orig_defaced.nii.gz')):
                shutil.copy(sourcefile,destindir)
                print('\n --- %s: T1_orig_defaced.nii.gz copied --- \n'%subjname)
            else:
                print('\n --- %s: T1_orig_defaced.nii.gz already exists --- \n'%subjname)

# Print arguments for user
def print_dirs(var1,var2,var3,var4,var5):
    print('\n ---Following are the arguments you chose--- \n')
    print('\n Work directory: %s \n'%var1)
    print('\n Wildcard: %s \n'%var2)
    print('\n Output directory: %s \n'%var3)
    print('\n CAT12 standalone directory: %s \n'%var4)
    print('\n MATLAB runtime compiler directory: %s'%var5)

# prepare the textfile and submit the jobs
def submit_cat_seg(maindir,outdir,catpath,mcrmainpath):    
    niipath      = glob.glob(os.path.join(maindir,'**',outdir,'T1_orig_defaced.nii.gz'),recursive=True)
    mcrpath      = os.path.join(mcrmainpath,"v93") # v93 is must for MATLAB 2017b compiler
    textfilename = 'cat_segmentation_jobs.txt'
    textfile     = os.path.join(maindir,'cat_segmentation_jobs.txt')
    if os.path.exists(textfile):
        newfile = textfilename.replace(".txt","_old.txt")
        shutil.move(textfile,os.path.join(maindir,newfile))    
    df=open(textfile,'w')    
    for niis in niipath:       
        command = os.path.join(catpath,"cat_standalone.sh") + " -m " + mcrpath + " -b " + os.path.join(catpath,"cat_standalone_segment_enigma.m") + " " + niis
        df.write(command)
        df.write('\n')
    df.close()
    # Submit array tasks using fsl_sub
    os.chdir(maindir)
    command =  "fsl_sub -q short.q -t " + "./cat_segmentation_jobs.txt"
    print(command)
    os.system(command)

# define main function
def main():
    parser = argparse.ArgumentParser(description='--Run CAT12 segmentation--')
    parser.add_argument("maindir", help = 'Parent directory name')
    parser.add_argument("wildcard", help = 'Wildcard initials')
    parser.add_argument("outdir",help='Output directory name')
    parser.add_argument("catpath",help="CAT12 standalone directory path")
    parser.add_argument("mcrmainpath",help="path to MATLAB runtime MCR")    
    args        = parser.parse_args()
    maindir     = args.maindir
    wildcard    = args.wildcard
    outdir      = args.outdir
    catpath     = args.catpath
    mcrmainpath = args.mcrmainpath    
    print_dirs(maindir,wildcard,outdir,catpath,mcrmainpath)    
    prepare_cat(maindir,wildcard,outdir)
    submit_cat_seg(maindir,outdir,catpath,mcrmainpath)
    
if __name__ == "__main__":
    main()    
