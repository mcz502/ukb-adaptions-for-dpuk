% maxNumCompThreads('automatic')

% maindir  = '/vols/Data/psychiatry/BHC/derivatives/preprocessing';

maindir  = '/Users/psyc1586_admin/r_vols/psychiatry/BHC/whitehall_defaced/cat12_T1w_whitehall';
outname  = 'group_cat';
wildcard = 'WH';


if ~exist(fullfile(maindir,outname),'dir')
    mkdir(fullfile(maindir,outname));
end

subjects = dir(fullfile(maindir, sprintf('%s*',wildcard)));  
allmat   = cell(length(subjects),1);


for subjs = 1:length(subjects)
    subjdir = fullfile(subjects(subjs).folder,subjects(subjs).name);
    matfile = dir(fullfile(subjdir,'cat12_qc','CAT12.8.1','report','cat*.mat'));

    if ~isempty(matfile) && length(matfile)==1 && ~exist(fullfile(subjdir,'cat12_qc','CAT12.8.1','err'),'dir')
        allmat{subjs,1} = fullfile(matfile(1).folder,matfile(1).name);
    else
        allmat{subjs,1} = [];
    end
end

selectedMats =  allmat(~cellfun('isempty',allmat));
outfile      = fullfile(maindir,outname,sprintf('cat12_qa_%s.csv', datestr(now,'mm-dd-yyyy_HH-MM-SS')));
files        = selectedMats;

% These lines are taken from cat_main.m (lines 2051-2054; CAT 12.5 v1363)
grades      = {'A+','A','A-','B+','B','B-','C+','C','C-','D+','D','D-','E+','E','E-','F'};
mark2rps    = @(mark) min(100,max(0,105 - mark*10)) + isnan(mark).*mark;
mark2grad   = @(mark) grades{min(numel(grades),max(max(isnan(mark)*numel(grades),1),round((mark+2/3)*3-3)))};

% Initialize
num_files = length(files);

header = {'filename'; ...
          'resolution_RMS_value'; 'resolution_RMS_rps';    'resolution_RMS_grade'; ...
          'noise_NCR_value';      'noise_NCR_rps';         'noise_NCR_grade';      ...
          'bias_ICR_value';       'bias_ICR_rps';          'bias_ICR_grade';       ...
          'weighted_IQR_value';   'weighted_IQR_rps';      'weighted_IQR_grade';   ...
          'surface_euler_num';    'size_topology_defects'; 'SurfaceIntensityRMSE';  ...
          'SurfacePositionRMSE';   'version_MATLAB';       'version_SPM';          ...
          'version_CAT';            'vol_abs_WMH';         'vol_rel_WMH';          ...
          'vol_abs_CSF';            'vol_abs_GM' ;         'vol_abs_WM';           ...
          'vol_rel_CSF';           'vol_rel_GM' ;          'vol_rel_WM'; 'vol_TIV'};
      
measures = cell(num_files, length(header));

% Get values
for file = 1:num_files
    
    str = load(files{file});
    str = str.S;

    % Filename
    measures{file,1}  = files{file};
    
    % Resolution
    try
        measures{file,2}  = str.qualityratings.res_RMS;
        measures{file,3}  = mark2rps(str.qualityratings.res_RMS);
        measures{file,4}  = mark2grad(str.qualityratings.res_RMS);
        
        % Noise
        measures{file,5}  = str.qualityratings.NCR;
        measures{file,6}  = mark2rps(str.qualityratings.NCR);
        measures{file,7}  = mark2grad(str.qualityratings.NCR);
        
        % Bias
        measures{file,8}  = str.qualityratings.ICR;
        measures{file,9}  = mark2rps(str.qualityratings.ICR);
        measures{file,10} = mark2grad(str.qualityratings.ICR);
        
        % Weighted IQR
        measures{file,11} = str.qualityratings.IQR;
        measures{file,12} = mark2rps(str.qualityratings.IQR);
        measures{file,13} = mark2grad(str.qualityratings.IQR);
        
        try
            % Surface Euler number
            measures{file,14} = str.subjectmeasures.EC_abs;
            
            % Size of topology defects
            measures{file,15} = str.subjectmeasures.defect_size;

            measures{file,16} = str.subjectmeasures.qualitymeasures.SurfaceIntensityRMSE;
            measures{file,17} = str.subjectmeasures.qualitymeasures.SurfacePositionRMSE;
            
        catch
            % Surface Euler number
            measures{file,14} = NaN;
            
            % Size of topology defects
            measures{file,15} = NaN;

            measures{file,16} = NaN;
            measures{file,17} = NaN;
        end
        
        % MATLAB version
        measures{file,18} = num2str(str.software.version_matlab);
        
        % SPM version
        measures{file,19} = num2str(str.software.version_spm);
        
        % CAT version
        measures{file,20} = [num2str(str.software.version_cat), ' v', ...
                             num2str(str.software.revision_cat)];
                         
        % Other additional subject measures: adding more measures
        measures{file,21} = round(str.subjectmeasures.vol_abs_WMH,3);
        measures{file,22} = round(str.subjectmeasures.vol_rel_WMH,3);
        measures{file,23} = round(str.subjectmeasures.vol_abs_CGW(:,1),3);
        measures{file,24} = round(str.subjectmeasures.vol_abs_CGW(:,2),3);
        measures{file,25} = round(str.subjectmeasures.vol_abs_CGW(:,3),3);
        measures{file,26} = round(str.subjectmeasures.vol_rel_CGW(:,1),4);
        measures{file,27} = round(str.subjectmeasures.vol_rel_CGW(:,2),4);
        measures{file,28} = round(str.subjectmeasures.vol_rel_CGW(:,3),4);
        measures{file,29} = round(str.subjectmeasures.vol_TIV,3);

    
        % Clear up for the next file
        clear str
    end
end

% Convert to table and save as csv file
measures = cell2table(measures, 'VariableNames', header);
writetable(measures,outfile)


