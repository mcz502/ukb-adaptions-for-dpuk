%% 
maindir = '/Users/psyc1586_admin/GVB_data/Whitehall_fullTestdata';
outdir  = fullfile(maindir,'whitehall_InputforFileManager');

if ~exist(outdir,'dir')
    mkdir(outdir)
end

%%

reldirs = {'ASL','DTI','FLAIR','RESTING','STRUCTURAL','T2'};

% Get all relevant directories only (modalities)
dirs    = dir(fullfile(maindir,'*'));
moddirs =  dirs(ismember({dirs.name},reldirs));


for mods = 1:length(moddirs)
    fulldir = fullfile(moddirs(mods).folder,moddirs(mods).name);
    cd(fulldir)
    
    subjdirs = dir(fullfile(fulldir,'WH*'));

    for subjs = 1:length(subjdirs)
        subdir  = fullfile(subjdirs(subjs).folder,subjdirs(subjs).name);
        subname = subjdirs(subjs).name;
        
        if ~exist(fullfile(outdir,subname),'dir')
            mkdir(fullfile(outdir,subname))
            files = dir(fullfile(subdir,'*nii*'));
            
            if strcmpi(moddirs(mods).name,'DTI') == 1
                bvals = dir(fullfile(subdir,'bvals'));
                for ff=1:length(bvals)
                    movefile(fullfile(bvals(ff).folder,bvals(ff).name),fullfile(outdir,subname));
                end
                bvecs = dir(fullfile(subdir,'bvecs'));
                for ff=1:length(bvecs)
                    movefile(fullfile(bvecs(ff).folder,bvecs(ff).name),fullfile(outdir,subname));
                end
            end

            for ff=1:length(files)
                movefile(fullfile(files(ff).folder,files(ff).name),fullfile(outdir,subname));
            end

        else
            fprintf('\n %s: Directory already exists..Concatenating..\n',subname);
            files = dir(fullfile(subdir,'*nii*'));
            
            if strcmpi(moddirs(mods).name,'DTI') == 1
                bvals = dir(fullfile(subdir,'bvals'));
                for ff=1:length(bvals)
                    movefile(fullfile(bvals(ff).folder,bvals(ff).name),fullfile(outdir,subname));
                end
                bvecs = dir(fullfile(subdir,'bvecs'));
                for ff=1:length(bvecs)
                    movefile(fullfile(bvecs(ff).folder,bvecs(ff).name),fullfile(outdir,subname));
                end
            end
            
            for ff=1:length(files)
                movefile(fullfile(files(ff).folder,files(ff).name),fullfile(outdir,subname));
            end
        end
    end
end
