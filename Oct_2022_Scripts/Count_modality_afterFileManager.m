%% Count the number of files in each directory after BHC file manager python script (for sanity or other works)
maindir   = '/Users/psyc1586_admin/GVB_data/BHC_fullTestdata/TestData';
subjdir   = dir(fullfile(maindir,'W*'));
subjdirs  = subjdir(~ismember({subjdir.name},{'.','..'}));
seldirs   = {'ASL','dMRI','fMRI','raw','SWI','T1','T2_FLAIR'};
counTable = cell(length(subjdirs),length(seldirs));

for subj = 1:length(subjdirs)
    
    subjname = subjdirs(subj).name;
    fullsubj = fullfile(subjdirs(subj).folder,subjdirs(subj).name);
    
    cd(fullsubj)

    for ii=1:length(seldirs)

        alldir = fullfile(fullsubj,seldirs{ii});

        if ~exist(alldir,'dir')
            counTable{subj,ii} = 'NA';
        else
            cd(alldir)
            files = dir(fullfile(alldir,'**/*.nii.gz'));
            counTable{subj,ii} = length(files);
        end
    end
end

%%
fullTab                          = cell2table(counTable);
fullTab.Properties.VariableNames = seldirs';
subjnames                        = struct2cell(subjdirs);
subjnames1                       = subjnames(1,:)';
fullTab.subjects                 =  subjnames1;
fullTab                          = movevars(fullTab,'subjects','Before',seldirs{1});

[a,~,~] = fileparts(maindir);
writetable(fullTab,fullfile(a,sprintf('modality_counts_%s.csv', datestr(now,'mm-dd-yyyy_HH-MM-SS'))))