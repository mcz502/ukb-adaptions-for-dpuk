#!/usr/bin/env python
# coding: utf-8

# In[14]:

import os
import sys
import glob
import shutil
import subprocess 
import shlex
from subprocess import Popen
import argparse
import json
import re


# After running participant level mriqc in subject specific BIDS folder; 
# for group level bring everything to a single directory where each subject is has respective jsons in BIDS
# and .htmls 
           
# Example execution of the function for given subject's directory
# maindir         = "/home/fs0/mcz502/SampleData2/Moredata/test_mriqc"
# outdir          = "/home/fs0/mcz502/SampleData2/Moredata/test_mriqc/groupBIDS"
# wilcard         = Wildcard for filenames in quotes; for example 'W' for BHC data
# mriqc_dirname   = "BIDS"
# mriqc_sing_img = '/vols/Scratch/mcz502/mriqc_singularity/mriqc-0.15.1.simg'

def prepare_group_mriqc(maindir,outdir,wildcard,mriqc_dirname):
    subjdirs = glob.glob(os.path.join(maindir,wildcard + '*'))
    
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        
    for subj in subjdirs:
        subjdir = os.path.basename(subj)
        print(subjdir)
        
        htmls = glob.glob(os.path.join(maindir,subjdir,mriqc_dirname,'*.html'))
        for files in htmls:
            print('\n %s --- Moving the participant level outputs of mriqc ---\n'%subjdir)              
            try:  
                shutil.copy(files,outdir)
            except:
                print('\n -%s---Could not copy htmls...Reasons - 1) mriqc participant-level not successful 2) files already moved \n' %subjdir)


        bidsdir   = outdir

        ifbidspresent = os.path.exists(bidsdir)

        if ifbidspresent == False:
            print('%s: Creating BIDS directory' % subjdir)        
            os.mkdir(bidsdir)

        if(bool(re.search('_',subjdir))==True):
            print("%s: invalid subject name" % subjdir)
            newsubjname = subjdir.replace("_","")
            print(subjdir)        
            newsubjdir = os.path.join(bidsdir,"sub-" + newsubjname)        
            ifsubjpresent = os.path.exists(newsubjdir)
            
            if ifsubjpresent == False:
                print('%s: Changing directory name to: %s' % (subjdir,"sub-" + newsubjname))
                os.mkdir(newsubjdir)
                
             # Handle T1 files
            anatdir       = os.path.join(newsubjdir,'anat')    
            ifanatpresent = os.path.exists(anatdir)    
            if ifanatpresent == False:    
                os.mkdir(anatdir)            
                sourcefile = os.path.join(maindir,subjdir,'T1','T1.nii.gz')
                destinfile = os.path.join(anatdir,"sub-" + newsubjname + "_T1w.nii.gz")
                try:
                    shutil.copy(sourcefile.replace('.nii.gz','.json'),destinfile.replace('.nii.gz','.json'))
                except:
                    print('\n files not found in anatdir \n') 

            # Handle rest fMRI files & edit json with task name    
            funcdir       = os.path.join(newsubjdir,'func')
            iffuncpresent = os.path.exists(funcdir)
            if iffuncpresent == False:
                os.mkdir(funcdir)
                sourcefile = os.path.join(maindir,subjdir,'fMRI','rfMRI.nii.gz')
                destinfile = os.path.join(funcdir,"sub-" + newsubjname + "_task-rest_bold.nii.gz")
                try:
                    shutil.copy(sourcefile.replace('.nii.gz','.json'),destinfile.replace('.nii.gz','.json'))
                    json_file_path = destinfile.replace('.nii.gz','.json')
                except:
                    print('\n files not found in funcdir \n')

        else:
            print("%s: valid subject name" % subjdir)
            newsubjname = subjdir
            print(subjdir)
            newsubjdir = os.path.join(bidsdir,"sub-" + subjdir)        
            ifsubjpresent = os.path.exists(newsubjdir)
            
            if ifsubjpresent == False:
                print('%s: Changing directory name to: %s' % (subjdir,"sub-" + newsubjname))
                os.mkdir(newsubjdir)

            # Handle T1 files
            anatdir       = os.path.join(newsubjdir,'anat')    
            ifanatpresent = os.path.exists(anatdir)    
            if ifanatpresent == False:    
                os.mkdir(anatdir)
                sourcefile = os.path.join(maindir,subjdir,'T1','T1.nii.gz')
                destinfile = os.path.join(anatdir,"sub-" + newsubjname + "_T1w.nii.gz")
                try:
                    shutil.copy(sourcefile.replace('.nii.gz','.json'),destinfile.replace('.nii.gz','.json'))
                except:
                    print('\n files not found in anatdir \n')                
               
                
            # Handle rest fMRI files & edit json with task name    
            funcdir       = os.path.join(newsubjdir,'func')
            iffuncpresent = os.path.exists(funcdir)
            if iffuncpresent == False:
                os.mkdir(funcdir)
                sourcefile = os.path.join(maindir,subjdir,'fMRI','rfMRI.nii.gz')
                destinfile = os.path.join(funcdir,"sub-" + newsubjname + "_task-rest_bold.nii.gz")
                try:
                    shutil.copy(sourcefile.replace('.nii.gz','.json'),destinfile.replace('.nii.gz','.json'))
                    json_file_path = destinfile.replace('.nii.gz','.json')
                except:
                    print('\n files not found in funcdir \n')

def main():
    parser = argparse.ArgumentParser(description='--Run group-MRIQC & MRIQC-classifier--')
    parser.add_argument("maindir",       help = 'Parent Directory Name')
    parser.add_argument("outdir",        help = 'Output Directory Name')
    parser.add_argument("wildcard",      help = 'character/s')
    parser.add_argument("mriqc_dirname", help = "Directory name where mriqc-participant level is stored")
    parser.add_argument("mriqc_sing_img",help = "MRIQC singularity image path")
    
    args            = parser.parse_args()   
    
    maindir         = args.maindir    
    outdir          = args.outdir
    wildcard        = args.wildcard
    mriqc_dirname   = args.mriqc_dirname
    mriqc_sing_img  = args.mriqc_sing_img
            
    # Prepare for group mriqc
    prepare_group_mriqc(maindir,outdir,wildcard,mriqc_dirname)

    # Run group mriqc
    command_group = "singularity run -B /Scratch:/Scratch " + mriqc_sing_img + " " + outdir + " " + outdir + " group"
    print(command_group)
    os.system(command_group)

    # Run mriqc-classifier
    group_T1_tsv       = os.path.join(outdir,'group_T1w.tsv')
    os.chdir(outdir)
    command_classifier = "singularity run -B /Scratch:/Scratch" + mriqc_sing_img + " " + "mriqc_clf --load-classifier -X " + group_T1_tsv
    print(command_classifier)
    os.system(command_classifier)     
    
if __name__ == "__main__":
    main()

