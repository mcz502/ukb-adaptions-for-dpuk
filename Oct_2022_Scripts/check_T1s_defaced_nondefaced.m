%%
mydir = '/Users/psyc1586_admin/r_vols/psychiatry/BHC/derivatives/preprocessing';
subjs = dir(fullfile(mydir,'W3T*'));
outs  = cell(length(subjs),11);

for sub = 1:length(subjs)
    subjdir  = fullfile(subjs(sub).folder,subjs(sub).name);
    subjname = subjs(sub).name;
    
    cd(fullfile(subjdir,'T1'))

    outs{sub,1} = subjname;
    
    % T1
    if exist(fullfile(subjdir,'T1','T1.nii.gz'),'file')
        outs{sub,2} = 1;
        img     = niftiread(fullfile(subjdir,'T1','T1.nii.gz'));
        [a,b,c] = size(img);
        outs{sub,3} = append(num2str(a),'x',num2str(b),'x',num2str(c)); % size of T1.nii.gz
        outs{sub,4} = sum(img(:)>100); % total voxels with intensity greater than 100
    else
        outs{sub,2} = 0;
        outs{sub,3} = 0;
        outs{sub,4} = 0;
    end
    clear img a b c 
    
    % T1_orig
    if exist(fullfile(subjdir,'T1','T1_orig.nii.gz'),'file')
        outs{sub,5} = 1;
        img     = niftiread(fullfile(subjdir,'T1','T1_orig.nii.gz'));
        [a,b,c] = size(img);
        outs{sub,6} = append(num2str(a),'x',num2str(b),'x',num2str(c)); % size of T1_orig.nii.gz
        outs{sub,7} = sum(img(:)>100); % total voxels with intensity greater than 100
    else
        outs{sub,5} = 0;
        outs{sub,6} = 0;
        outs{sub,7} = 0;
    end
    clear img a b c
    
    % T1_orig_defaced
    if exist(fullfile(subjdir,'T1','T1_orig_defaced.nii.gz'),'file')
        outs{sub,8} = 1;
        img     = niftiread(fullfile(subjdir,'T1','T1_orig_defaced.nii.gz'));
        [a,b,c] = size(img);
        outs{sub,9} = append(num2str(a),'x',num2str(b),'x',num2str(c)); % size of T1_orig_defaced.nii.gz
        outs{sub,10} = sum(img(:)>100); % total voxels with intensity greater than 100
    else
        outs{sub,8}  = 0;
        outs{sub,9}  = 0;
        outs{sub,10} = 0;
    end
    clear img a b c

    % json associated with T1.nii.gz     
    if exist(fullfile(subjdir,'T1','T1.json'),'file')
        outs{sub,11} = 1;
    else
        outs{sub,11} = 0;
    end
end

% Create table
table_outs = cell2table(outs);
table_outs.Properties.VariableNames = {'subjname', 'T1_avail','T1_size','T1_voxels',...
                                        'T1_orig_avail','T1_orig_size','T1_orig_voxels',...
                                        'T1_orig_defaced_avail','T1_orig_defaced_size','T1_orig_defaced_voxels',...
                                        'T1_json_avail'};

    

