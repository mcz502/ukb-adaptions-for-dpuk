function get_qa_cat(files,outfile)
% Function to read CAT report XML or .mat files and extract quality 
% assurance measures, percentages, grades and other additional measures into a csv file

%% Inputs:
% files: cell with full paths to XML or .mat files (rows are filenames)

%% Check inputs and assign defaults
% Check files
if ~exist('files', 'var') || isempty(files)
    error('files should be provided');
end


%% Define functions
% These lines are taken from cat_main.m (lines 2051-2054; CAT 12.5 v1363)
grades      = {'A+','A','A-','B+','B','B-','C+','C','C-','D+','D','D-','E+','E','E-','F'};
mark2rps    = @(mark) min(100,max(0,105 - mark*10)) + isnan(mark).*mark;
mark2grad   = @(mark) grades{min(numel(grades),max(max(isnan(mark)*numel(grades),1),round((mark+2/3)*3-3)))};

%% Initialize
num_files = length(files);

header = {'filename'; ...
          'resolution_RMS_value'; 'resolution_RMS_rps'; 'resolution_RMS_grade'; ...
          'noise_NCR_value';      'noise_NCR_rps';      'noise_NCR_grade';      ...
          'bias_ICR_value';       'bias_ICR_rps';       'bias_ICR_grade';       ...
          'weighted_IQR_value';   'weighted_IQR_rps';   'weighted_IQR_grade';   ...
          'surface_euler_num';    'size_topology_defects';                      ...
          'version_MATLAB';       'version_SPM';        'version_CAT';          ...
          'WMH_abs'; 'WMH_rel'; 'WMH_WM_rel'; 'vol_abs_CGW'; 'vol_TIV'; 'vol_rel_CGW'};
      
measures = cell(num_files, length(header));

%% Get values
for file = 1:num_files
    
    % Check if XML or mat file
    ext  = strsplit(files{file}, '.');
    ext  = ext{end};
    if strcmpi(ext, 'xml')
        str  = cat_io_xml(files{file});
    else
        str = load(files{file}, 'S');
        str = str.S;
    end
    
    % Filename
    measures{file,1}  = files{file};
    
    % Resolution
    measures{file,2}  = str.qualityratings.res_RMS;
    measures{file,3}  = mark2rps(str.qualityratings.res_RMS);
    measures{file,4}  = mark2grad(str.qualityratings.res_RMS);
    
    % Noise
    measures{file,5}  = str.qualityratings.NCR;
    measures{file,6}  = mark2rps(str.qualityratings.NCR);
    measures{file,7}  = mark2grad(str.qualityratings.NCR);
    
    % Bias
    measures{file,8}  = str.qualityratings.ICR;
    measures{file,9}  = mark2rps(str.qualityratings.ICR);
    measures{file,10} = mark2grad(str.qualityratings.ICR);
    
    % Weighted IQR
    measures{file,11} = str.qualityratings.IQR;
    measures{file,12} = mark2rps(str.qualityratings.IQR);
    measures{file,13} = mark2grad(str.qualityratings.IQR);
    
    try
        % Surface Euler number
        measures{file,14} = str.subjectmeasures.EC_abs;
        
        % Size of topology defects
        measures{file,15} = str.subjectmeasures.defect_size;
        
    catch
        % Surface Euler number
        measures{file,14} = NaN;
        
        % Size of topology defects
        measures{file,15} = NaN;
    end
    
    % MATLAB version
    measures{file,16} = num2str(str.software.version_matlab);
    
    % SPM version
    measures{file,17} = num2str(str.software.version_spm);
    
    % CAT version
    measures{file,18} = [num2str(str.software.version_cat), ' v', ...
                         num2str(str.software.revision_cat)];
                     
    % Other additional subject measures: adding more measures
    measures{file,19} = round(str.subjectmeasures.WMH_abs,3);
    measures{file,20} = round(str.subjectmeasures.WMH_rel,3);
    measures{file,21} = round(str.subjectmeasures.WMH_WM_rel,3);
    measures{file,22} = round(str.subjectmeasures.vol_abs_CGW,3);
    measures{file,23} = round(str.subjectmeasures.vol_TIV,3);
    measures{file,24} = round(str.subjectmeasures.vol_rel_CGW,3);

    % Clear up for the next file
    clear str
end

%% Convert to table and save as csv file
measures = cell2table(measures, 'VariableNames', header);
writetable(measures,outfile)