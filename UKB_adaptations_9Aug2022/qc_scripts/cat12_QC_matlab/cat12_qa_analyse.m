%% Run CAT12 segmentation and get the Quality and other measures

% Run CAT12 segmentation

% Unzip nii.gz and fetch all the files in the main directory
% maindir  = '/Users/psyc1586_admin/GVB_data/sample_subjects/tests';
% surf_y = 1, surface processing ; surf_y = 0, no surface processing
% n_proc, number of parallel processes

function cat12_qa_analyse(maindir,wildcard,surf_y,n_proc)
% First arranging the data for CAT12 segmentation
    arrange_for_CATSeg(maindir,wildcard)
    
    filelist = dir(fullfile(maindir, '**/CAT12_qc_work/T1.nii.gz'));  
    allT1    = cell(length(filelist),1);
    
    for ff=1:length(filelist)
        allT1{ff,1} = fullfile(filelist(ff).folder,filelist(ff).name);
    end
    
    
    for ii=1:length(allT1)
        fprintf('\n.... Unzipping the nii.gz files \n ....')    
        gunzip(allT1{ii,1})
    end
    
    submitT1 = strrep(allT1,'.gz',',1');
    
    path2spm = which('spm');
    [a,~,~]  = fileparts(path2spm);
    
    % CAT12 options: kept as default; no surface processing; no parallel
    % processing
    clear matlabbatch
    matlabbatch{1}.spm.tools.cat.estwrite.data = submitT1;
    matlabbatch{1}.spm.tools.cat.estwrite.nproc = n_proc;
    matlabbatch{1}.spm.tools.cat.estwrite.opts.tpm = cellstr(fullfile(a,'tpm/TPM.nii'));
    matlabbatch{1}.spm.tools.cat.estwrite.opts.affreg = 'mni';
    matlabbatch{1}.spm.tools.cat.estwrite.opts.biasstr = 0.5;
    matlabbatch{1}.spm.tools.cat.estwrite.opts.accstr = 0.5;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.APP = 1070;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.NCstr = -Inf;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.LASstr = 0.5;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.gcutstr = 2;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.cleanupstr = 0.5;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.WMHC = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.SLC = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.segmentation.restypes.fixed = [1 0.1];
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.registration.dartel.darteltpm = cellstr(fullfile(a,'toolbox/cat12/templates_1.50mm/Template_1_IXI555_MNI152.nii'));
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.vox = 1.5;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.surface.pbtres = 0.5;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.surface.scale_cortex = 0.7;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.surface.add_parahipp = 0.1;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.surface.close_parahipp = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.admin.ignoreErrors = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.admin.verb = 2;
    matlabbatch{1}.spm.tools.cat.estwrite.extopts.admin.print = 2;
    matlabbatch{1}.spm.tools.cat.estwrite.output.surface = surf_y;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.neuromorphometrics = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.lpba40 = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.cobra = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.hammers = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.ibsr = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.aal = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.mori = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.ROImenu.atlases.anatomy = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.GM.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.GM.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.GM.mod = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.GM.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WM.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WM.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WM.mod = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WM.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.CSF.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.CSF.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.CSF.mod = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.CSF.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WMH.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WMH.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WMH.mod = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.WMH.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.SL.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.SL.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.SL.mod = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.SL.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.atlas.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.atlas.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.label.native = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.label.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.label.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.bias.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.bias.warped = 1;
    matlabbatch{1}.spm.tools.cat.estwrite.output.bias.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.las.native = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.las.warped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.las.dartel = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.jacobianwarped = 0;
    matlabbatch{1}.spm.tools.cat.estwrite.output.warps = [0 0];
    
    save(fullfile(maindir,'cat12_seg_batch.mat'),'matlabbatch')
    spm_jobman('run',matlabbatch)
    
    clear matlabbatch
    
    %% get all the quality measures 
    
    xmlist = dir(fullfile(maindir, '**/CAT12_qc_work/report/*.xml'));  
    allXml    = cell(length(xmlist),1);
    
    for ff=1:length(xmlist)
        allXml{ff,1} = fullfile(xmlist(ff).folder,xmlist(ff).name);
    end
    
    outfile = fullfile(maindir,sprintf('cat12_qa_%s.csv', datestr(now,'mm-dd-yyyy_HH-MM-SS')));
    get_qa_cat(allXml,outfile)
    
    %% get the GM, WM, CSF, WMH and TIV
    clear matlabbatch
    outfile = fullfile(maindir,sprintf('cat12_volumes_%s.csv', datestr(now,'mm-dd-yyyy_HH-MM-SS')));
    matlabbatch{1}.spm.tools.cat.tools.calcvol.data_xml = allXml;
    matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_TIV = 0;
    matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_name = outfile;
    save(fullfile(maindir,'cat12_vols_batch.mat'),'matlabbatch')
    spm_jobman('run',matlabbatch)

    %%
    fprintf('\n Finished all the CAT12 quality processing in %s \n', maindir)

end


