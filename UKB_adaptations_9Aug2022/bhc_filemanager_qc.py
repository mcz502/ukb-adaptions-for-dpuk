#!/usr/bin/env python
# coding: utf-8

# In[28]:


#!/bin/env python
#
# Script name: bb_file_manager_forBHC.py
#
# Description: Script to do the organisation of the files of a new dataset
#			   in UK Biobank file/directory format.

########################### START OF SCRIPT #########################

# Import, Provide data paths

script_path = "/Users/psyc1586_admin/GVB_data/ukb_scripts_ver1"
data_dir    = "/Users/psyc1586_admin/GVB_data/testdata/basic_test4"

import sys
import os
log_dir     = os.path.join(script_path,"bb_pipeline_tools")

sys.path.insert(0,script_path)
sys.path.insert(0,os.path.join(script_path,"bb_pipeline_tools"))

import re
import os
import glob
import time
import logging
import sys,argparse,os.path
import bb_logging_tool as LT
import bb_general_tools.bb_path as bb_path
import shutil
import json
import nibabel as nib
import copy
import numpy as np

functiondir = os.path.join(script_path,"custom_functions")

def formatFileConfig():
    result=""    
    for key in fileConfig:
        result=result + key + "\n"
        for value in fileConfig[key]:
            result=result + "  "  + value + "\n"
    return result

def generate_SBRef(origPath, outputPath):
    commandToRun=script_path+'/bb_functional_pipeline/bb_generate_SBRef ' + origPath + " " + outputPath
    logger.warn("There was no SBRef data for the subject " + origPath) 
    logger.warn("The SBRef data will be generated now using the middle point of the subject")
    logger.warn("Command to run: " + commandToRun )
    LT.runCommand(logger, commandToRun )     

def remove_phase_info(fileName):
    result=re.sub('_[PH]|_[ph].', '',fileName)
    return result

def remove_coil_info(fileName):
    result=re.sub('_COIL[0-9]*_', '_',fileName)
    return result

def remove_echo_info(fileName):
    result=re.sub('_ECHO.*_','_',fileName)
    return result

def rename_no_coil_echo_info(fileName):
    result=remove_coil_info(fileName)
    result=remove_echo_info(result)
    
    if (fileName != result):
        move_file(fileName, result)
    
    return result

def read_json(fileName):
    
    result={}

    if os.path.isfile(fileName):
        if bb_path.isImage(fileName):
            jsonFileName=bb_path.removeImageExt(fileName)+ '.json'
                
            if os.path.isfile(jsonFileName):
                with open(jsonFileName, 'r') as f:
                    result=json.load(f)
    return result

def get_image_json_field(fileName, field):
    
    result=[]
    jsonDict=read_json(fileName)

    if jsonDict != {}:
        result = jsonDict[field]
    
    return result

def save_acquisition_date_time(fileName):
    
    dateTime=get_image_json_field(fileName, 'AcquisitionDateTime')
    #20140831122443.796875
    #Format this date    

def image_type_contains(fileName, desiredType):

    imageType=get_image_json_field(fileName, 'ImageType')

    # 2 possible formats in the BIDS json file
    if isinstance(imageType, str):
        imageType=imageType.split('_')
    elif not isinstance(imageType, list):
        raise NameError('The content of the json file associated with ' + fileName + ' is incorrect')
    
    if desiredType in imageType:
        return True
 
    return False

def is_normalised(fileName):
    return image_type_contains(fileName, 'NORM')

def is_phase(fileName):
    return image_type_contains(fileName, 'P')

def move_to(listFiles, destination):
    for fileName in listFiles:
        move_file(fileName, destination+fileName)

# Convert all the file names to upper case to avoid 
# ambiguities (Extensions are always in lower case)
# Remove _ character at the beginning of the filename
def capitalize_and_clean(listFiles):

    logger.info('File names changed to upper case.')
    for fileName in listFiles:

        newFileName=fileName.upper()
        if newFileName.startswith("_"):
            newFileName=newFileName[1:]

        endings=[".NII.GZ", "BVAL", "BVEC", "JSON", "NII"] # Also added NII (GVB)
        
        for ending in endings:
            if newFileName.endswith(ending):
                newFileName=newFileName.replace(ending, ending.lower())

        os.rename(fileName,newFileName)

def move_file(oldPath, newPath):

    #The file may be a json file and may have been moved previously
    if os.path.isfile(oldPath):
        logger.info("File moved/renamed: " + oldPath + " to " + newPath)
        os.rename(oldPath, newPath)

        #If there is an associated json, move it as well
        if bb_path.isImage(oldPath):
            plainOrigName=bb_path.removeImageExt(oldPath)
            plainNewName=bb_path.removeImageExt(newPath)

            if os.path.isfile(plainOrigName + '.json'):
                os.rename(plainOrigName + '.json', plainNewName + '.json')

def move_file_add_to_config(oldPath, key, boolAppend):
    if boolAppend:
        move_file(oldPath, idealConfig[key] + "/" +oldPath)
        fileConfig[key].append(idealConfig[key] + "/" +oldPath)
    else:
        move_file(oldPath, idealConfig[key])
        fileConfig[key]=idealConfig[key]
        

# The flag parameter indicates whether this is T1 or T2_FLAIR
def manage_struct(listFiles, flag):
    
    #listFiles=robustSort(listFiles) # This creates empty listFiles hence not used (GVB)
    
    numFiles=len(listFiles)

    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    boolNorm=[is_normalised(x) for x in listFiles]

    if not any(boolNorm):
        logger.error("There was not an intensity-normalized " + flag + ".")
        if flag == "T1":        
            logger.error("It will not be possible to process the subject")
    else:
        indexLastNorm=(numFiles - list(reversed(boolNorm)).index(True)) -1
        normalisedFileName=listFiles[indexLastNorm]
        move_file_add_to_config(normalisedFileName, flag, False)
        listFiles.remove(normalisedFileName)
        boolNorm=boolNorm[:indexLastNorm]

        if False in boolNorm:

            indexLastNotNorm=(len(boolNorm) - list(reversed(boolNorm[:indexLastNorm])).index(False)) -1
            
            if indexLastNotNorm>=0:
                notNormalisedFileName=listFiles[indexLastNotNorm]  
                move_file_add_to_config(notNormalisedFileName, flag + "_notNorm", False)
                listFiles.remove(notNormalisedFileName)
 
    for fileName in listFiles:
        move_file(fileName, 'unclassified/'+fileName) 

# The flag parameter indicates whether this is resting or task fMRI
def manage_fMRI(listFiles, flag):
    
#     listFiles=robustSort(listFiles) # This creates empty listFiles hence not used (GVB)
    numFiles=len(listFiles)
    dim=[]

    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    # Get the dimensions for all the fMRI images
    for fileName in listFiles:
        epi_img=nib.load(fileName)
#         dim.append (epi_img.get_header()['dim'][4])
        dim.append (epi_img.header['dim'][4]) # modified (GVB)

    if numFiles == 0 :
        logger.warn("There was no " + flag + "FMRI data")
        
    elif numFiles == 1:
        # If the only fMRI we have is the SBRef
        if dim[0] == 1:
            logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing")
            move_file_add_to_config(listFiles[0], flag + "_SBRef", False)

        # If we have fMRI data but no SBRef, we generate it.         
        else:
            move_file_add_to_config(listFiles[0], flag, False)
            generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
            fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

    elif numFiles == 2:
        biggestImageDim=max(dim)
        indBiggestImage=dim.index(biggestImageDim)
        indSmallestImage=1 - indBiggestImage
        
        # If there is at least one propper fMRI image
        if biggestImageDim > 1:
            move_file_add_to_config(listFiles[indBiggestImage], flag, False)

            # If the other image is an SBRef image
            if dim[indSmallestImage] == 1:
                move_file_add_to_config(listFiles[indSmallestImage], flag + "_SBRef", False)
            
            # If not, forget about it and generate and SBRef
            else:
                generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
                fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

        else:
            logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing")
            move_file_add_to_config(listFiles[numFiles -1], flag + "_SBRef", False)

    # If there are more than 2 rfMRI images, and at least one has more than one volume, 
    # we will take the biggest one as the fMRI volume and generate take as SBRef the one
    # with the previous numeration. If that one is not a proper SBRef, generate it.
    elif (max(dim) > 1):
        indBiggestImage=dim.index(max(dim))
        move_file_add_to_config(listFiles[indBiggestImage], flag, False)

        
        fileName=listFiles[indBiggestImage]
        plainFileName=bb_path.removeImageExt(fileName)
        number=int(plainFileName.split("_")[-1])
        
        ind=-1

        for fileToCheck in listFiles:
            #Check if the file with the previous numeration is in the list
            numberToCheck=int(bb_path.removeImageExt(fileName).split("_")[-1])
            if numberToCheck == (number-1):
                ind=listFiles.index(fileToCheck)    

        
        # If there is a file with the file number that should correspond to this case
        if ind >0:
            # If the file with the previous numeration is a SBREF file
            if dim[ind] == 1:
                move_file_add_to_config(listFiles[ind], flag + "_SBRef", False)

            # If not, forget about it and generate a new one
            else:
                generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
                fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]     

        else:
            generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
            fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

    # There are several fMRI images but neither of them have more than one volume  
    else:
        logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing.")
        move_file_add_to_config(listFiles[numFiles -1], flag + "_SBRef", False)

        
def manage_DWI(listFiles):

#     listFiles=robustSort(listFiles) #This creates empty listFiles hence not used (GVB)
    numFiles=len(listFiles)
    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    subListFilesD={}
    imageFilesD={}

    if numFiles == 0:
        logger.error("There was no DWI data.  There will be no DWI processing.")        
        print("There was no DWI data.  There will be no DWI processing.")        
    else:
        errorFound=False
        encodingDirections=["PA", "AP"]    

        #Code needed for the inconsistency in the file names in Diffusion over the different phases
        if listFiles[0].startswith("MB3"):

            subListFilesD['PA']=[x for x in listFiles if x.find('PA') != -1]
            imageFilesD['PA']=[x for x in subListFilesD['PA'] if bb_path.isImage(x)]

            subListFilesD['AP']=[x for x in listFiles if x not in subListFilesD['PA']]
            imageFilesD['AP']=[x for x in subListFilesD['AP'] if bb_path.isImage(x)]

        else:
            for direction in encodingDirections:
                subListFilesD[direction]=[x for x in listFiles if x.find(direction) != -1]
                imageFilesD[direction]=[x for x in subListFilesD[direction] if bb_path.isImage(x)]
        try:

            for direction in encodingDirections:

               dim=[]
               
               subListFiles=subListFilesD[direction]
               imageFiles=imageFilesD[direction]

               for fileName in imageFiles:
                   epi_img=nib.load(fileName)
#                    dim.append(epi_img.get_header()['dim'][4])
                   dim.append(epi_img.header['dim'][4]) # modified (GVB)
               numImageFiles=len (imageFiles)

               if numImageFiles == 0 :
                   raise Exception("There should be at least one DWI image in the " + direction + 
                                   " direction with more than one volume. DWI data is not correct."
                                   " There will be no diffusion processing.")

               biggestImageDim=max(dim)
               indBiggestImage=dim.index(biggestImageDim)

               # There is no proper DWI image
               if biggestImageDim <=1:
                   raise Exception("There should be at least one DWI image in the " + direction + 
                                   " direction with more than one volume. DWI data is not correct."
                                   " There will be no diffusion processing.")

               if numImageFiles>1:
                   # Check if there is SBRef file for the direction
                   if (dim.count(1) == 0):
                        logger.warn("There was no SBRef file in the " + direction + " direction.")

                   # If there is at least one, take the last one.                    
                   else:

                       # Get the index of the last image with dimension = 1
                       indexSBRef=numImageFiles - list(reversed(dim)).index(1) -1
                       move_file_add_to_config(imageFiles[indexSBRef], direction + "_SBRef", False)
      

               # Take the biggest image in the selected direction and set it as the DWI image for that direction
               move_file_add_to_config(imageFiles[indBiggestImage], direction , False)

               # BVAL and BVEC files should have the same name as the image, changing the extension
               bvalFileName=bb_path.removeImageExt(imageFiles[indBiggestImage]) + ".bval"
               bvecFileName=bb_path.removeImageExt(imageFiles[indBiggestImage]) + ".bvec"

               if (not bvalFileName in subListFiles) or (not bvecFileName in subListFiles):
                   raise Exception("There should be 1 bval and 1 bvec file in " + direction + 
                                   " direction. DWI data is not correct. There will be no"
                                   " diffusion processing.")

               move_file_add_to_config(bvecFileName, direction + "_bvec", False)
               move_file_add_to_config(bvalFileName, direction + "_bval", False)

        # In case of any big error in the data, set DWI data as inexistent.
        except Exception as e:
            for key in ["AP", "AP_bval", "AP_bvec", "PA", "PA_bval", "PA_bvec" ]:
                fileConfig[key]=""
            logger.error(str(e))

        # Set the rest of the files as unclassified
        for fileName in listFiles:
            if os.path.isfile(fileName):
                move_file(fileName, "unclassified/"+fileName)

# Code of manage_SWI is changed throughout to meet the needs with BHC data

def manage_SWI(listFiles):

#     listFiles=robustSort(listFiles) # This creates empty listFiles hence not used (GVB)
    numFiles=len(listFiles)

    #Chnaged from 134 to 132 files; BHC data doen't have 2 non-normalized scans but ukb does

    if numFiles <=131:
        logger.error("There should be at least 132 SWI files. Only " + str(numFiles) + " found. There will be no SWI processing")
        print("There should be at least 132 SWI files. Only " + str(numFiles) + " found. There will be no SWI processing")
    elif numFiles >132:
        logger.error("The number of SWI files (" + str(numFiles) + ") is incorrect. There will be no processing")
        print("The number of SWI files (" + str(numFiles) + ") is incorrect. There will be no processing")
    else:
        
        mainFiles=[x for x in listFiles if ("_CH" in x)] # modified to CH instead of COIL (GVB)
        
        if mainFiles:
            for key in ["SWI_MAG_TE1", "SWI_MAG_TE2", "SWI_PHA_TE1", "SWI_PHA_TE2" ]:
                fileConfig[key]=[]


            # Classifying coil files
            for fileName in mainFiles:

                boolPhase=is_phase(fileName)
                #boolPhase= bb_path.removeImageExt(fileName).endswith('_PHA') 
                boolTE1 = (fileName.find('_E1') != -1) # modified to E1 instead of ECHO1 (GVB)

                if boolPhase:
                    if boolTE1:
                        move_file_add_to_config(fileName, "SWI_PHA_TE1", True)
                    else:
                        move_file_add_to_config(fileName, "SWI_PHA_TE2", True)

                else:
                    if boolTE1:
                        move_file_add_to_config(fileName, "SWI_MAG_TE1", True)

                    else:
                        move_file_add_to_config(fileName, "SWI_MAG_TE2", True)

            for mainFile in mainFiles:
                listFiles.remove(mainFile)

            for mainFile in listFiles:

                #boolPhase=is_phase(mainFile)
                boolPhase= bb_path.removeImageExt(mainFile).endswith('_PH') 
                boolTE1=(mainFile.find('_E1') != -1) # modified to E1 instead of ECHO1 (GVB)

                if not boolTE1:
                    boolTE = (mainFile.find('_ECHO1_') != -1) # Also added ECHO1 where there is no E1 (GVB)

                if boolPhase:
                    if boolTE1:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_PHA", False)
                    else:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_PHA_TE2", False)

                else:
                    if boolTE1:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_MAG", False)

                    else:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_MAG_TE2", False)
        else:
            
            mainFiles = [x for x in listFiles if ("COIL" in x)] # modified to CH instead of COIL (GVB)
            noCHfiles = [x for x in listFiles if ("COILHEA" in x)]
            
            for noCHfile in noCHfiles:
                mainFiles.remove(noCHfile)
        
            
            for key in ["SWI_MAG_TE1", "SWI_MAG_TE2", "SWI_PHA_TE1", "SWI_PHA_TE2" ]:
                fileConfig[key]=[]


            # Classifying coil files
            for fileName in mainFiles:

                boolPhase=is_phase(fileName)
                #boolPhase= bb_path.removeImageExt(fileName).endswith('_PHA') 
                boolTE1 = (fileName.find('ECHO1') != -1) # modified to E1 instead of ECHO1 (GVB)

                if boolPhase:
                    if boolTE1:
                        move_file_add_to_config(fileName, "SWI_PHA_TE1", True)
                    else:
                        move_file_add_to_config(fileName, "SWI_PHA_TE2", True)

                else:
                    if boolTE1:
                        move_file_add_to_config(fileName, "SWI_MAG_TE1", True)

                    else:
                        move_file_add_to_config(fileName, "SWI_MAG_TE2", True)

            for noCHfile in noCHfiles:

                #boolPhase=is_phase(mainFile)
                boolPhase= bb_path.removeImageExt(noCHfile).endswith('_PH') 
                boolTE1=(noCHfile.find('_ECHO1_') != -1) # modified to E1 instead of ECHO1 (GVB)

                if boolPhase:
                    if boolTE1:
                        move_file_add_to_config(noCHfile, "SWI_TOTAL_PHA", False)
                    else:
                        move_file_add_to_config(noCHfile, "SWI_TOTAL_PHA_TE2", False)

                else:
                    if boolTE1:
                        move_file_add_to_config(noCHfile, "SWI_TOTAL_MAG", False)

                    else:
                        move_file_add_to_config(noCHfile, "SWI_TOTAL_MAG_TE2", False)    

# doing for ASL seperatey since it is not part of ukbb data
def manage_ASL(listFiles,flag):         
    
    numFiles=len(listFiles)

    if numFiles == 0:
        logger.warn('\n %s: There is no ASL data for this subject \n' % subject)
        print('\n %s: There is no ASL data for this subject \n' % subject)
    else:
        for file in listFiles:
            loadfile = nib.load(file)
            dims     = loadfile.header['dim'][4]
#             print(file,dims)

            if dims == 99:
                statusNORM = is_normalised(file)
                if statusNORM == 1:
                    print('\n %s: ASL data found \n' % subject)
#                     if not os.path.exists(os.path.join(data_dir,subject,"ASL")):
#                         os.makedirs(os.path.join(data_dir,subject,"ASL"))
                    move_file_add_to_config(file, flag, False)
            else:
                move_file(file, "unclassified/"+file)
                print('\n %s: ASL data found but does not contain 99 volumes \n' % subject)    
    
# Additional function specific to nii files in the parent directory: Compress NIfTI (nii to nii.gz) (GVB)
def compressNII_files(data_dir,subject,bashScriptName,functionname):
    
    os.chdir(os.path.join(data_dir,subject))
    listFiles = glob.glob(os.path.join(data_dir,subject,"*.nii"))
    if not listFiles:
        print("\n %s: No .nii files found...checking if nii.gz exist \n" % subject)

        listgz = glob.glob(os.path.join(data_dir,subject,"*.nii.gz"))
        if not listgz:
            print("\n %s: Also, no .nii.gz files found...check if files are already organized \n" % subject) 
    else:
        print("\n %s: %d nii files found...we need to compress them \n" % (subject,len(listFiles)))
        indir    = os.path.join(data_dir,subject)
        funcWdir = os.path.join(functiondir,bashScriptName + ".sh")
        command  = funcWdir + " " + functionname + " " + indir
        os.system(command)
        print("\n %s: The .nii files are being compressed to .nii.gz..This may take sometime.. \n" % subject)
        
# Addiitonal function to Organize, handle and sort the clinical SWI in BHC data (GVB)
def sort_SWI(data_dir,subject):
    
    os.chdir(os.path.join(data_dir,subject))            
    listFiles=glob.glob("*.*")
    listFiles.sort()
    swi_types = ('*swi*.nii.gz','swi*.nii.gz')
    listFiles = []
    for pat in swi_types:
        listFiles.extend([x for x in glob.glob(pat) if x not in listFiles])
    if not os.path.exists(os.path.join(data_dir,subject,"clinicalSWI")):
        os.makedirs(os.path.join(data_dir,subject,"clinicalSWI"))

    # Separating clinical SWI from research SWI using series description field in json
    for files in listFiles:
        imageType=get_image_json_field(files, 'SeriesDescription')
        if "_P_RR" not in imageType:
            try:
                shutil.move(os.path.join(data_dir,subject,files),os.path.join(data_dir,subject,"clinicalSWI"))
                shutil.move(os.path.join(data_dir,subject,files.replace(".nii.gz",".json")),os.path.join(data_dir,subject,"clinicalSWI"))
                print("\n %s: Found _P_RR tag in json; moving %s file to clinicalSWI" % (subject,os.path.basename(files)))
            except:
                print("\n %s: May be no clinicalSWI found w.r.t _P_RR tag in json; going ahead...\n" % subject)
                
# Changed throughout to match needs of BHC data (GVB)
def bb_file_manager_forBHC(subject):
    
    global logger
    global idealConfig
    global fileConfig
    
    # Now changing nii to nii.gz; the compression-way otherwise it will have problem to load in nibabel
    compressNII_files(data_dir,subject,"compress_niis","compress_nii")    
        
    sort_SWI(data_dir,subject)
    
    os.chdir(os.path.join(data_dir,subject))
    listFiles=glob.glob("*.*")
    listFiles.sort()
    
    os.chdir(data_dir)
        
    logger = LT.initLogging(os.path.join(script_path,"bb_logging_tool.py"), subject) 


    idealConfigFile=script_path + '/bb_data/ideal_config_bhc.json' # modified with script_path (GVB)
    
    with open(idealConfigFile, 'r') as f:
        idealConfig=json.load(f)
        
    directories=["delete", "unclassified", "raw", "T1", "T2_FLAIR", "SWI", 
            "SWI/PHA_TE1", "SWI/PHA_TE2", "SWI/MAG_TE1", "SWI/MAG_TE2", 
            "SWI/unclassified", "dMRI", "dMRI/raw", "fMRI", "fieldmap","ASL"]
    
    # Wildcards changed throughout to match the needs of BHC data (GVB)
    patterns_actions = [[["*.*"], capitalize_and_clean],
                        [["dicom","DICOM"], move_to, "delete/"],
                        [["*T1*.nii.gz","T1*.nii.gz","*T1*.nii","T1*.nii"], manage_struct, "T1"], 
                        [["*T2_FLAIR*.nii.gz","T2_FLAIR*.nii.gz","*T2_FLAIR*.nii","T2_FLAIR*.nii"], manage_struct, "T2"], 
                        [["MB8_FMRI*RESTING*.nii.gz","MB8_FMRI*RESTING*.nii","*MB8_FMRI*RESTING*.nii.gz","*MB8_FMRI*RESTING*.nii","*MB8_FMRI*RESTING.nii.gz","*MB8_FMRI*RESTING.nii","MB8_FMRI*RESTING.nii.gz","MB8_FMRI*RESTING.nii"], manage_fMRI, "rfMRI"], 
                        [["*FMRI*TASK*.nii.gz", "MB8*TASK*.nii.gz" ], manage_fMRI, "tfMRI"],
                        [["*SWI*.nii","SWI*.nii","*SWI*.nii.gz","SWI*.nii.gz"],manage_SWI], 
                        [["*DIFF*MB3*","DIFF*MB3*"],manage_DWI],
                        [["*ASL*.nii.gz"],manage_ASL,"ASL"],
                        [["*.*"], move_to, "unclassified/"]
                       ]
    
    os.chdir(os.path.join(data_dir, subject)) 
    fd_fileName="logs/file_descriptor.json"

    if (os.path.isfile(fd_fileName)):
        with open(fd_fileName, 'r') as f:
            fileConfig=json.load(f)
    
    else:
    
        for directory in directories:
            if not os.path.isdir(directory):         
                os.mkdir(directory)

        listFiles=glob.glob("*.*")
        listFiles.sort()

        # Organize the files in sets
        for patterns_action in patterns_actions:
            patterns = patterns_action[0]
            action   = patterns_action[1]
            args     = patterns_action[2:]
            listFiles=[]            
            for fileTy in patterns:
                listFiles.extend([x for x in glob.glob(fileTy) if x not in listFiles])
    #             logger.info("Performing action " + action.__name__ + " on files with patterns " + str(patterns))
            action(listFiles, *args)
        
        # Create file descriptor
        fd=open(fd_fileName, "w")
        json.dump(fileConfig,fd,sort_keys=True,indent=4)        
        fd.close()
        
    os.chdir("..")
    fileConfigFormatted=formatFileConfig()
    return fileConfig

# Loop through all the subjects in data_dir (GVB)
os.chdir(data_dir)

subjects = glob.glob(os.path.join(data_dir,'*'))

for subj in subjects:
    logger=None
    idealConfig={}
    fileConfig={}
    subject      = os.path.basename(subj)
    print("\n ------ %s: Starting with file manager ------ \n" % subject)          
    fileConfig   = bb_file_manager_forBHC(subject)
    print("\n ------ %s: Done with file manager ------ \n" % subject)
    
########################### END OF SCRIPT #########################    


# In[29]:


# Handling the files in unclassified folder; sorting them into modality-specific directories; 
# These may be handled later for other analyses or inclusion to BIDS, etc.

# Loop through all the subjects in data_dir (GVB)
os.chdir(data_dir)

subjects = glob.glob(os.path.join(data_dir,'W*'))

for subj in subjects:
    subject         = os.path.basename(subj)
    unclassifiedDir = os.path.join(data_dir,subject,'unclassified')
    os.chdir(unclassifiedDir)
    
    print('\n -----Started handling the unclassified files in %s -----' % subject)
    # Handle localiser files
    localiserImages = glob.glob(os.path.join(unclassifiedDir,'*LOCALISER*'))
    
    numfiles = len(localiserImages)
    
    if numfiles == 0:
        print('\n %s: No localiser files found \n' % subject)
    else:
        print('\n %s: Localiser files found...moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"Localiser")):
            os.mkdir(os.path.join(unclassifiedDir,"Localiser"))        
        for localiser in localiserImages:
            sourceImg  = localiser
            destinDir  = os.path.join(unclassifiedDir,"Localiser")
            shutil.move(sourceImg,destinDir)
    
    # Handle T1w files
    T1Images = glob.glob(os.path.join(unclassifiedDir,'*T1*'))
    
    numfiles = len(T1Images)
    
    if numfiles == 0:
        print('\n %s: No T1 files found \n'% subject)
    else:
        print('\n %s: T1 files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"T1")):
            os.mkdir(os.path.join(unclassifiedDir,"T1"))
        for T1 in T1Images:
            sourceImg  = T1
            destinDir  = os.path.join(unclassifiedDir,"T1")
            shutil.move(sourceImg,destinDir)    
    
    # Handle T1w files
    FLAIRImages = glob.glob(os.path.join(unclassifiedDir,'*FLAIR*'))
    
    numfiles = len(FLAIRImages)
    
    if numfiles == 0:
        print('\n %s: No FLAIR files found \n'% subject)
    else:
        print('\n %s: FLAIR files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"FLAIR")):
            os.mkdir(os.path.join(unclassifiedDir,"FLAIR"))
        for FLAIR in FLAIRImages:
            sourceImg  = FLAIR
            destinDir  = os.path.join(unclassifiedDir,"FLAIR")
            shutil.move(sourceImg,destinDir)
        
    # Handle resting files
    restImages = glob.glob(os.path.join(unclassifiedDir,'*REST*'))
    
    numfiles = len(restImages)
    
    if numfiles == 0:
        print('\n %s: No rest-fMRI files found \n'% subject)
    else:  
        print('\n %s: Rest-fMRI files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"RfMRI")):
            os.mkdir(os.path.join(unclassifiedDir,"RfMRI"))
        for rest in restImages:
            sourceImg  = rest
            destinDir  = os.path.join(unclassifiedDir,"RfMRI")
            shutil.move(sourceImg,destinDir)
        
    # Handle diffusion files
    diffImages = glob.glob(os.path.join(unclassifiedDir,'*DIFF*'))
    
    numfiles = len(diffImages)
    
    if numfiles == 0:
        print('\n %s: No diffusion files found \n'% subject)
    else:
        print('\n %s: Diffusion files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"DIFF")):
            os.mkdir(os.path.join(unclassifiedDir,"DIFF"))
        for diff in diffImages:
            sourceImg  = diff
            destinDir  = os.path.join(unclassifiedDir,"DIFF")
            shutil.move(sourceImg,destinDir)  
        
    # Handle TOF*NECK files
    neckImages = glob.glob(os.path.join(unclassifiedDir,'*TOF*'))
    
    numfiles = len(neckImages)
    
    if numfiles == 0: 
        print('\n %s: No TOF*NECK files found \n'% subject)
    else:
        print('\n %s: TOF*NECK files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"TOF_NECK")):
            os.mkdir(os.path.join(unclassifiedDir,"TOF_NECK"))
        for neck in neckImages:
            sourceImg  = neck
            destinDir  = os.path.join(unclassifiedDir,"TOF_NECK")
            shutil.move(sourceImg,destinDir)  
            
    # Handle ASL files
    neckImages = glob.glob(os.path.join(unclassifiedDir,'*ASL*'))
    
    numfiles = len(neckImages)
    
    if numfiles == 0: 
        print('\n %s: No ASL files found \n'% subject)
    else:
        print('\n %s: ASL files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"ASL")):
            os.mkdir(os.path.join(unclassifiedDir,"ASL"))
        for neck in neckImages:
            sourceImg  = neck
            destinDir  = os.path.join(unclassifiedDir,"ASL")
            shutil.move(sourceImg,destinDir)
            
    # Handle SWI files
    swiImages = glob.glob(os.path.join(unclassifiedDir,'*SWI*'))
    
    numfiles = len(swiImages)
    
    if numfiles == 0: 
        print('\n %s: No SWI files found \n'% subject)
    else:
        print('\n %s: SWI files found..moving them \n'% subject)
        if not os.path.exists(os.path.join(unclassifiedDir,"SWI")):
            os.mkdir(os.path.join(unclassifiedDir,"SWI"))
        for swi in swiImages:
            sourceImg  = swi
            destinDir  = os.path.join(unclassifiedDir,"SWI")
            shutil.move(sourceImg,destinDir)        
    
    # Also, copy the clinicalSWI folder to unclassified
    shutil.move(os.path.join(data_dir,subject,"clinicalSWI"),unclassifiedDir)
    
    
    print('\n -----Finished handling the unclassified files in %s -----' % subject)


# In[30]:


def bb_basic_QC_forBHC(subject):

    keysToPop=[]
    global logger
    
    os.chdir(data_dir)
    
    logger = LT.initLogging("bb_basic_QC_forBHC.py", subject) 

    idealConfigFile=script_path + '/bb_data' + '/ideal_config_sizes_BHC.json' # modified json (GVB)
    with open(idealConfigFile, 'r') as f:
        idealConfig=json.load(f)

    os.chdir(subject)
    fd_fileName="logs/file_descriptor.json"

    for fileN in fileConfig:
        if not isinstance(fileConfig[fileN], list):

            if bb_path.isImage(fileConfig[fileN]):
                fils=bb_path.removeImageExt(fileConfig[fileN])   
      
                if os.path.isfile(fils+"_orig.nii.gz"):
                    fileList=[fils+"_orig.nii.gz"]
                else:
                    fileList=[fileConfig[fileN]]

            else:
                fileList=[fileConfig[fileN]]
        else:
            fileList=fileConfig[fileN]
 
        for fileName in fileList:
            if os.path.isfile(fileName):
                if fileN in idealConfig:
                    img = nib.load(fileName)
                    dims = img.header['dim'][1:5]
                    if not np.all(dims == idealConfig[fileN]['dims']):
                        keysToPop.append(fileN)
                        #make_unusable(fileName, idealConfig[fileName]['dep_dirs'])
                        f = open('info_basic_QC.txt', 'a')
                        f.write('Problem in file ' + fileName+'\n')
                        f.close()

    for keyToPop in keysToPop:
        fileConfig.pop(keyToPop,None)

    fd=open(fd_fileName, "w")
    json.dump(fileConfig,fd,sort_keys=True,indent=4)        
    fd.close()

    os.chdir("..")

    return fileConfig

# Loop through all the subjects in data_dir (GVB)
os.chdir(data_dir)

subjects = glob.glob(os.path.join(data_dir,'*'))

for subj in subjects:
    subject      = os.path.basename(subj)
    fileConfig   = bb_basic_QC_forBHC(subject) 


# In[ ]:




